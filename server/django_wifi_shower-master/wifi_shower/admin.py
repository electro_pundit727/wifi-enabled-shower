from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Device, Profile, User, ShoweringData
from .forms import CustomUserChangeForm, CustomUserCreationForm


class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
         ),
    )
    list_display = ('email', 'is_active', 'date_joined')
    search_fields = ('email',)
    ordering = ('email',)


class DeviceAdmin(admin.ModelAdmin):
    fields = ('mac_id', 'secret_key', 'created_date', 'sold_date', 'user')
    readonly_fields = ('created_date', 'secret_key')
    list_display = ('mac_id', 'secret_key', 'created_date', 'sold_date', 'user')


class ProfileAdmin(admin.ModelAdmin):
    fields = ('user', 'name', 'last_shower_date', 'old_shower_habits', 'shower_cycle', 'gallons_saved',
              'aggregate_shower_savings', 'shower_temp', 'challenge_level', 'created_date')
    readonly_fields = ('shower_cycle', 'old_shower_habits', 'gallons_saved', 'shower_temp', 'aggregate_shower_savings',
                       'challenge_level', 'created_date', 'last_shower_date')
    list_display = ('user', 'name', 'last_shower_date', 'old_shower_habits', 'shower_cycle', 'gallons_saved',
                    'aggregate_shower_savings', 'shower_temp', 'challenge_level', 'created_date')


class ShoweringDataAdmion(admin.ModelAdmin):
    fields = ('device', 'user', 'profile', 'created_date', 'shower_mode', 'preheat_cycle', 'old_shower_habits',
              'shower_cycle', 'shower_temp', 'gallons_used', 'gallons_saved', 'average_shower_time',
              'aggregate_shower_savings', 'average_shower_savings', 'challenge_level', 'challenge_time', 'status')
    readonly_fields = ('device', 'user', 'profile', 'created_date', 'shower_mode', 'preheat_cycle', 'old_shower_habits',
                       'shower_cycle', 'shower_temp', 'gallons_used', 'gallons_saved', 'average_shower_time',
                       'aggregate_shower_savings', 'average_shower_savings', 'challenge_level', 'challenge_time', 'status')
    list_display = ('device', 'user', 'profile', 'created_date', 'shower_mode', 'preheat_cycle', 'old_shower_habits',
                    'shower_cycle', 'shower_temp', 'gallons_used', 'gallons_saved', 'average_shower_time',
                    'aggregate_shower_savings', 'average_shower_savings', 'challenge_level', 'challenge_time', 'status')


admin.site.register(User, CustomUserAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(ShoweringData, ShoweringDataAdmion)
