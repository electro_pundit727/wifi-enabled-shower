from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/hello')
def hello_world():
    return 'Hello World!'


@app.route('/profile', methods=['POST'])
def get_showeringData():
    req_data = request.get_json()

    preheat_cycle = req_data.get('Preheat cycle')
    shower_duration = req_data.get('Duration of the shower')
    gallons = req_data.get('Gallons used')
    alarm = req_data.get('Alarm')

    return '''
    The preheat cycle is: {}
    The duration of the shower is: {}
    The gallons used is: {}
    The alarm is: {}
    '''.format(preheat_cycle, shower_duration, gallons, alarm)


@app.route('/command', methods=['GET'])
def send_command():
    return jsonify(dict(command="waiting", challenge_level=5))


if __name__ == '__main__':
    app.run(host='0.0.0.0')
