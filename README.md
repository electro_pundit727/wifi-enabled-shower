This project is to design next generation shower.

Next Generation shower valve is an electro-mechanical valve assembly that automates much of the manual discretionary adjustments that occur at the beginning and intermittently throughout the showering event. 

By doing this, Next Generation can completely eliminate water that is wasted during the warm up and temperature adjustments that occurs at the beginning of every shower. In addition, Next Generation is Wi-Fi enabled which allows the user to fully customize their showering experience.

This ensures that 100% of the water dispensed is dedicated to comfort while simultaneously maximizing conservation. All shower water conservation devices on the market today sacrifice comfort in order to achieve conservation.

Next Generation is the first showering device that increases both comfort and conservation.