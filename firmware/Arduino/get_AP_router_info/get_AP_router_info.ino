#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <EEPROM.h>

int PIN_STATE_LED = 5;   // Indicates whether the data was received in AP mode, the device was connected the WiFi router in STA mode. (GPIO5)

String ssid, pwd;

char router_ssid[25];
char device_pwd[25];

int s_len, p_len;  // length of WiFi router's ssid and password
int recv_flg;      // flag for WiFi router info receving status 

uint8_t macAddr[6];
char MAC_char[18];

// WiFiClient espClient;
WiFiServer server(80);

void Setup_AP() {
  // configure a AP on ESP8266 and build a server on it
  Serial.println();
  Serial.println("Configuring access point...");

  WiFi.mode(WIFI_AP);

  WiFi.softAPmacAddress(macAddr);
  for (int i = 0; i < sizeof(macAddr); ++i) {
    sprintf(MAC_char, "%s%02x", MAC_char, macAddr[i]);
  }
  Serial.println();
  Serial.println(MAC_char);
  String macID(MAC_char);
  Serial.println(macID);

  macID.toUpperCase();

  String AP_NameString = "Shower_" + macID;
  String AP_PassString = "Shower-" + macID;

  char AP_NameChar[AP_NameString.length() + 1];
  memset(AP_NameChar, 0, AP_NameString.length() + 1);
  for (int i = 0; i < AP_NameString.length(); i++)
    AP_NameChar[i] = AP_NameString.charAt(i);

  char AP_PassChar[AP_PassString.length() + 1];
  memset(AP_PassChar, 0, AP_PassString.length() + 1);
  for (int i = 0; i < AP_PassString.length(); i++)
    AP_PassChar[i] = AP_PassString.charAt(i);

  Serial.println(AP_NameChar);
  Serial.println(AP_PassChar);

  WiFi.softAP(AP_NameChar, AP_PassChar);

  IPAddress myIP = WiFi.softAPIP();
  Serial.println();
  Serial.print("AP IP address: ");
  Serial.println(myIP);

  server.begin();
  Serial.println("HTTP server started");
}

void setup() {
  // pins setup
  pinMode(PIN_STATE_LED, OUTPUT);
  digitalWrite(PIN_STATE_LED, LOW);
  
  Serial.begin(115200);
  EEPROM.begin(512);  // store values of relay state and brightness level setting  

  Setup_AP();
}

void Router_Info_EEPROM_Write(String ssid, String pwd);

void loop() {

  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  if (recv_flg == 0) {
    digitalWrite(PIN_STATE_LED, !digitalRead(PIN_STATE_LED));
    delay(100);
  }
  // receive the data from the mobile app
  String Router_Info = client.readStringUntil('\r');  
  Serial.println(Router_Info);
  client.flush();

  // Parse the received data and get the ssid and password of the AP router
  int ssidIndex = Router_Info.indexOf("/start_ssid/");
  int pwdIndex = Router_Info.indexOf("/start_pwd/");
  int endIndex = Router_Info.indexOf("/end_info/");
  if (ssidIndex != -1) {
    ssid = "";
    pwd = "";
    for (int i = ssidIndex + 12; i < pwdIndex; i++) {
      ssid += Router_Info[i];
    }
    for (int i = pwdIndex + 11; i < endIndex; i++) {
      pwd += Router_Info[i];
    }
    Serial.println(ssid);
    Serial.println(pwd);
    recv_flg = 1;
    digitalWrite(PIN_STATE_LED, HIGH);

    // Write the WiFi router's SSID and Password to ESP8266's EEPROM
    Router_Info_EEPROM_Write(ssid, pwd);
  }
  client.flush();
  
  // JSON response
  String s = "HTTP/1.1 200 OK\r\n";
  s += "Content-Type: application/json\r\n\r\n";
  s += "{\"SSID\":\"";
  s += ssid;
  s += "\", \"PASSWORD\":\"";
  s += pwd;
  s += "\"}\r\n";
  s += "\n";
  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println("Client disconnected");
}

void Router_Info_EEPROM_Write(String ssid, String pwd) {

  s_len = ssid.length();
  p_len = pwd.length();
  EEPROM.write(0, s_len);
  EEPROM.write(1, p_len);

  for (int i = 0; i < s_len; i++) {
    EEPROM.write(0x02 + i, ssid[i]);
  }
  for (int i = 0; i < p_len; i++) {
    EEPROM.write(0x02 + s_len + i, pwd[i]);
  }
  EEPROM.commit();
  Serial.println("The SSID and Password of the WiFi router are saved in the EEPROM.");
}
