/**************Summary******************/
/* In this code we used the HTTP request to communicate with the server.
   To achieve the high requirements for the Security and the Certification Systme, we will need to use MQTT request
   than HTTP request.
   We will develop a new version that uses the MQTT protocol to communicate with the server.
   The new version will be named shower_control.cpp */

#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include <Adafruit_MCP23017.h>
#include <SPI.h>
#include "Adafruit_MAX31855.h"
#include <Ticker.h>

/********************** define PINs ************************/
// ESP8266-12E pins
int WiFi_LED = 15;           // GPIO15 -- Indicates whether the data was received in AP mode, the device was connected the WiFi router in STA mode.
int MODE_PIN = 3;            // GPIO3 -- pin for switching between AP mode and STA mode 
int esp8266IntPin = 13;      // GPIO13 -- ESP8266-12E Interrupt pin. This pin connected to the interrupt pin on MCP23017.
// MCP 23017 pins
byte Interrupt_START_Butt = 6;     // GPA6 -- START button                
byte Interrupt_Hot_Butt =  5;      // GPA5 -- Hot button 
byte Interrupt_Cold_Butt = 4;      // GPA4 -- Cold button
byte START_Led = 7;                // GPA7 -- START LED
byte Circ_Pump = 11;               // GPB3 -- Circulation Pump
byte Mix_Valve_hot = 10;           // GPB2 -- Mixing vavle relay for Hot water
byte Mix_Valve_cold = 9;           // GPB1 -- Mixing vavle relay for Cold water
byte Outf_Valve = 8;               // GPB0 -- Outflow valve

/*********************** define global variables ************************/
String ssid, pwd;
char router_ssid[25];
char router_pwd[25];
int s_len, p_len;              // length of WiFi router's ssid and password
int flg_recv_router_info;      // indicate if the device receved the WiFi router info from the mobile app

uint8_t macAddr[6];
char MAC_char[18];

const char* command = "waiting";          // command for starting the shower("waiting" and "starting")
const char* mode = "normal";              // shower mode("normal" and "challenge")
int chall_level;                          // challenge level in the challenge mode
int flg_timer1 = 0, flg_timer2 = 0;       // active state of the timer for the challenge mode

int flg_recv_comm;                // indicate if the command is received successfully by device
// int flg_start;                 // indicate if the "start" command is received from App.

int depressed_state = 0;    // Number of times the "START" button has been pressed. That indicates the working step of the shower.
float T_pump, T_mix;        // temperature of the circulation valve and the mixing valve
int ratio_mix = 6, ratio_mix_last = 6;     // ratio between the hot and cold water on the mixing valve  
                                           // preset ratio is 6 (60% hot to 40% cold)
int flg_preheat, flg_counter_shower;           // indicate if the "Zero Waste/Preheat Routine" and the shower is completed.
unsigned long preheat_start_time, preheat_end_time,    // start time and end time of "Zero Waste/Preheat Routine"
              shower_start_time, shower_end_time;      // start time and end time of the shower       
long preheat_cycle, shower_cycle;
bool flg_counter_preheat = false;
bool flg_counter_shower = false;

long lastMsg = 0;
/*********************** define constant values *************************/
int T_preset_min = 85;                  // preset temperature for "Zero Waste/Preheat Routine" (85F~95F)
int T_preset_max = 95;
int upload_interval = 1000;             // Uploading switch state in miliseconds.

/************************* define instances *******************************/
/*** For Adafruit MAX31855 ***/
// We will use two MAX31855 moudles for this project.
// Default connection is using software SPI, but comment and uncomment one of the two examples below to switch between software SPI and hardware SPI:
// Example creating a thermocouple instance with software SPI on any three digital IO pins.
// #define MAXDO   3
// #define MAXCS   4
// #define MAXCLK  5
// Adafruit_MAX31855 thermocouple(MAXCLK, MAXCS, MAXDO);

// Example creating a thermocouple instance with hardware SPI on a given CS pin.
#define MAXCS1 0       // GPIO0 -- first module's CS signal  
#define MAXCS2 2       // GPIO2 -- second module's CS signal 
Adafruit_MAX31855 thermocouple1(MAXCS1);
Adafruit_MAX31855 thermocouple2(MAXCS2);

/*** For MCP23017 ***/
Adafruit_MCP23017 mcp;

/*** For MCP23017 ***/
Ticker challenge1, challenge2;      // challenge1: timer to count shower time except the last minite
                                    // challenge2: timer to count the last minute

/*** build a server on ESP8266-12E ***/
WiFiServer server(80);

void setup() {
    
    Serial.begin(115200);
    EEPROM.begin(512);

    pinMode(MODE_PIN, INPUT);
    pinMode(WiFi_LED, OUTPUT);
    digitalWrite(WiFi_LED, LOW);

    if (digitalRead(MODE_PIN) == HIGH) {
        Setup_AP();
    }
    else {
        Setup_STA();
    }   
    
    // Interrupt pins 
    pinMode(esp8266IntPin, INPUT);
    mcp.begin(); 
    mcp.setupInterrupts(true, false, LOW);
    
    mcp.pinMode(Interrupt_START_Butt, INPUT);    
    mcp.pullUp(Interrupt_START_Butt, HIGH);
    mcp.setupInterruptPin(Interrupt_START_Butt, CHANGE);
    mcp.pinMode(Interrupt_Hot_Butt, INPUT);
    mcp.pullUp(Interrupt_Hot_Butt, HIGH);
    mcp.setupInterruptPin(Interrupt_Hot_Butt, CHANGE);
    mcp.pinMode(Interrupt_Cold_Butt, INPUT);
    mcp.pullUp(Interrupt_Cold_Butt, HIGH);
    mcp.setupInterruptPin(Interrupt_Cold_Butt, CHANGE);

    attachInterrupt(esp8266IntPin, handle_ButtInterrupt, FALLING); 
    mcp.readGPIOAB();            //This will clear interrupts on MCP prior to entering main loop

    // Normal I/O pins
    mcp.pinMode(START_Led, OUTPUT);    
    mcp.pinMode(Circ_Pump, OUTPUT);
    mcp.pinMode(Mix_Valve_hot, OUTPUT);
    mcp.pinMode(Mix_Valve_cold, OUTPUT);
    mcp.pinMode(Outf_Valve, OUTPUT);

    digitalWrite(START_Led, LOW);    
    digitalWrite(Circ_Pump, LOW);
    digitalWrite(Outf_Valve, LOW);
    // set the mixing valve to the full cold position at beginning    
    digitalWrite(Mix_Valve_hot, LOW);
    digitalWrite(Mix_Valve_cold, HIGH);
    delay(2000);
    digitalWrite(Mix_Valve_cold, LOW);    
    // preset the mixing valve to the 60% hot to 40% cold ratio
    // We assume that it takes 150ms to rotate the valve ball by 10% of the full range.
    digitalWrite(Mix_Valve_hot, HIGH);
    delay(900);  // motor energising time = 150ms x 6 = 900ms
    digitalWrite(Mix_Valve_hot, LOW); 

    // Initialize Ticker 
    // challenge1.attach(100, handle_ChallengeInterrupt1);  
    // challenge1.detach(); 
    // challenge2.attach(100, handle_ChallengeInterrupt2);  
    // challenge2.detach(); 

    ESP.wdtDisable();
    ESP.wdtEnable(WDTO_8S);
}

void Setup_AP() {
  
    // configure a AP on ESP8266 and build a server on it
    Serial.println();
    Serial.println("Configuring access point...");

    WiFi.mode(WIFI_AP);

    WiFi.softAPmacAddress(macAddr);
    for (int i = 0; i < sizeof(macAddr); ++i) {
        sprintf(MAC_char, "%s%02x", MAC_char, macAddr[i]);
    }
    Serial.println();
    Serial.println(MAC_char);
    String macID(MAC_char);
    Serial.println(macID);

    macID.toUpperCase();

    String AP_NameString = "Shower_" + macID;
    String AP_PassString = "Shower-" + macID;

    char AP_NameChar[AP_NameString.length() + 1];
    memset(AP_NameChar, 0, AP_NameString.length() + 1);
    for (int i = 0; i < AP_NameString.length(); i++)
        AP_NameChar[i] = AP_NameString.charAt(i);

    char AP_PassChar[AP_PassString.length() + 1];
    memset(AP_PassChar, 0, AP_PassString.length() + 1);
    for (int i = 0; i < AP_PassString.length(); i++)
        AP_PassChar[i] = AP_PassString.charAt(i);

    Serial.println(AP_NameChar);
    Serial.println(AP_PassChar);

    WiFi.softAP(AP_NameChar, AP_PassChar);

    IPAddress myIP = WiFi.softAPIP();
    Serial.println();
    Serial.print("AP IP address: ");
    Serial.println(myIP);

    server.begin();
    Serial.println("HTTP server started");
}

void Setup_STA() {

    s_len = EEPROM.read(0);
    p_len = EEPROM.read(1);

    for (int i = 0; i < s_len; i++) {
        ssid = ssid + char(EEPROM.read(0x02 + i));
    }
    for (int i = 0; i < p_len; i++) {
        pwd = pwd + char(EEPROM.read(0x02 + s_len + i));
    }

    ssid.toCharArray(router_ssid, s_len + 1);
    pwd.toCharArray(router_pwd, p_len + 1);

    WiFi.mode(WIFI_STA);

    Serial.println();
    Serial.print("router_ssid: ");
    Serial.println(router_ssid);
    Serial.print("router_pwd: ");
    Serial.println(router_pwd);

    int attempt = 0;
    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(router_ssid);

    WiFi.begin(router_ssid, router_pwd);

    while (WiFi.status() != WL_CONNECTED) {
        if (attempt < 30)
            attempt ++;
        else
            ESP.restart();
        delay(500);
        Serial.print(".");
        digitalWrite(WiFi_LED, !digitalRead(WiFi_LED));
    }

    digitalWrite(WiFi_LED, HIGH);
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());    
}

void Router_Info_EEPROM_Write(String ssid, String pwd) {

    s_len = ssid.length();
    p_len = pwd.length();
    EEPROM.write(0, s_len);
    EEPROM.write(1, p_len);

    for (int i = 0; i < s_len; i++) {
        EEPROM.write(0x02 + i, ssid[i]);
    }
    for (int i = 0; i < p_len; i++) {
        EEPROM.write(0x02 + s_len + i, pwd[i]);
    }
    EEPROM.commit();
    Serial.println("The SSID and Password of the WiFi router are saved in the EEPROM.");
}

void Get_Temp() {

    // basic readout test, just print the current temp
    // Serial.print("Internal Temp1 = ");
    // Serial.println(thermocouple1.readInternal());
    // Serial.print("Internal Temp2 = ");
    // Serial.println(thermocouple2.readInternal());

    // double T_pump = thermocouple1.readCelsius();
    // double T_mix = thermocouple2.readCelsius();    
    double T_pump = thermocouple1.readFarenheit();
    double T_mix = thermocouple2.readFarenheit();

    if (isnan(T_pump)) {
        Serial.println("Something wrong with thermocouple on the circulation pump.");
    } 
    else {
        Serial.print("T_pump = "); 
        Serial.print(T_pump);
        Serial.println("F");
    }
    if (isnan(T_mix)) {
        Serial.println("Something wrong with thermocouple on the mixing valve");
    } 
    else {
        Serial.print("T_mix = "); 
        Serial.println(T_mix);
        Serial.println("F");
    }    
    delay(10);
}

void handle_ButtInterrupt() {  /* We can add a code to limit the interrupt inteval time */

    Serial.println("Interrupt is just detected!");
    uint8_t pin = mcp.getLastInterruptPin();
    uint8_t val = mcp.getLastInterruptPinValue();

    if ((pin == Interrupt_START_Butt) && (val == LOW)) {
        Serial.println("START button is depressed!");
        depressed_state ++;
        if ((depressed_state == 2) && (flg_preheat == 0)) {
            Serial.println("Error! The 'Zero Waste/Preheat Routine' is still running.");
            depressed_state = 1;   
        }
        if (depressed_state > 3) 
            depressed_state = 3;       
    }
    if ((pin == Interrupt_Hot_Butt) && (val == LOW)) {     
        // The ratio can be changed only in the showering.
        if (depressed_state == 2) {
            Serial.println("Hot button is depressed. The hot water ratio will be incresed by 10 percent!");
            ratio_mix ++;            
            if (ratio_mix > 10) 
                ratio_mix = 10; 
            Serial.print("Current mixing ratio: ");                            
            Serial.print("hot->");
            Serial.print(ratio_mix * 10);
            Serial.print("percent, ");
            Serial.print("cold->");
            Serial.print((10 - ratio_mix) * 10);
            Serial.println("percent");                             
        }
        else {
            Serial.println("Error! You can setup the mixing ratio only while showering.");
        }
    }
    if ((pin == Interrupt_Cold_Butt) && (val == LOW)) {        
        // The ratio can be changed only in the showering.
        if (depressed_state == 2) {
            Serial.println("Cold button is depressed. The cold water ratio will be decreased by 10 percent!");
            ratio_mix --;
            if (ratio_mix < 0) 
                ratio_mix = 0;                             
            Serial.print("Current mixing ratio: ");                            
            Serial.print("hot->");
            Serial.print(ratio_mix * 10);
            Serial.print("percent, ");
            Serial.print("cold->");
            Serial.print((10 - ratio_mix) * 10);
            Serial.println("percent");
        }
        else {
            Serial.println("Error! You can setup the mixing ratio only while showering.");
        }
    }
}

void handle_ChallengeInterrupt1() {
    
    // This interrupt will be called when the timer comes within 1 minite of completion in the Challenge Routine mode
    Serial.println(" Hurry up! The shower will be finished automatically in a minute.");
    // close the outflow valve for 2 seconds, and then open the valve again.
    digitalWrite(Outf_Valve, LOW);
    delay(2000);
    digitalWrite(Outf_Valve, HIGH);
    flg_timer2 = 1;    
}

void handle_ChallengeInterrupt2() {
    
    Serial.println("The challenge mode is over!");
    digitalWrite(Outf_Valve, LOW);
    digitalWrite(START_Led, LOW);
    
    depressed_state ++;      // Then depressed_state will be 3.
}

void Receive_Command() {
  
    HTTPClient http;    // declare object of class HTTPClient
    http.begin("http://192.168.1.151:5000/command");  

    int httpCode = http.GET();
    String payload = http.getString();
    Serial.print("The received data: ");
    Serial.println(payload);

    if (httpCode == HTTP_CODE_OK) {
        flg_recv_comm = 1;
        Serial.println("The command is received successfully!");
        Send_ShoweringData("response"); 
        //Parse the received data
        Parsing_Command(payload);
    }
    else {
        flg_recv_comm = 0;
        Serial.println(httpCode);
        Serial.println("The command not receved successfully. Check Internet Connection or Server Error.");
        Send_ShoweringData("response"); 
    }      
    http.end();  //Close connection
    }

void Parsing_Command(String cmd) {

    // const size_t bufferSize = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(8) + 370;
    // DynamicJsonBuffer jsonBuffer(bufferSize);
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(cmd);
    // Parameters
    command = root["command"]; 
    chall_level = root["challenge level"];
    mode = root["mode"];

    Serial.print("command: ");
    Serial.println(command);
    Serial.print("challenge level: ");
    Serial.println(chall_level);
}

void Send_ShoweringData(String type) {

    // convert the showering data as JSON format
    StaticJsonBuffer<300> JSONbuffer;   // declaring static JSON buffer
    JsonObject& JSONencoder = JSONbuffer.createObject();
    if (type == "data") {
        JSONencoder["Preheat cycle"] = preheat_cycle;
        JSONencoder["Duration of the shower"] = shower_cycle;
        JSONencoder["Gallens used"] = 700;
    } 
    else if (type == "alarm") {
        if (flg_preheat == 1) 
            JSONencoder["Alarm"] = "Shower is Ready!";    
        else 
            JSONencoder["Alarm"] = "Shower is not yet Ready.";
    }
    else if (type == "response") {
        if (flg_recv_comm == 1)
        JSONencoder["Comm_response"] = "Success";
        else
        JSONencoder["Comm_response"] = "Failure";
    } 

    char JSONmessageBuffer[300];
    JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    Serial.println(JSONmessageBuffer);

    // send the sensor data to the sever
    HTTPClient http;    // declare object of class HTTPClient
    http.begin("http://192.168.1.151:5000/profile");                                                              
    http.addHeader("Content-Type", "application/json");       

    int httpCode = http.POST(JSONmessageBuffer);             // Send the request
    String payload = http.getString();                       // Get the response payload                           
    
    if (httpCode == HTTP_CODE_OK) {      
        Serial.println(httpCode);   //Print HTTP return code
        Serial.println("The showering data is posted to the sever successfully.");
    }
    else {
        Serial.println(httpCode);
        Serial.println("The posting to the server faile! Check Internet Connection or Server Error.");
        Serial.println(payload);    //Print request response payload
    }      
    http.end();  //Close connection
}

void reconnecting() {
  
    Serial.println("Now is trying to reconnect...");
    // Loop until we're reconnected
    int attempt = 0;
    while (WiFi.status() != WL_CONNECTED) {
        if (attempt < 30)
            attempt ++;
        else
            ESP.restart();
        delay(500);
        Serial.print(".");
        digitalWrite(WiFi_LED, !digitalRead(WiFi_LED));
    }

    digitalWrite(WiFi_LED, HIGH);
    Serial.println("");
    Serial.println("WiFi reconnected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
}

void loop() {

    if (MODE_PIN == HIGH) {  /************ In the "AP" mode *************/

        if (flg_recv_router_info == 0) {
            digitalWrite(WiFi_LED, !digitalRead(WiFi_LED));
            delay(100);
        }
        WiFiClient client = server.available();
        if (!client) {
            return;
        }
        // receive the data from the mobile app
        String Router_Info = client.readStringUntil('\r');  
        Serial.println(Router_Info);
        client.flush();

        // Parse the received data and get the ssid and password of the AP router
        int ssidIndex = Router_Info.indexOf("/start_ssid/");
        int pwdIndex = Router_Info.indexOf("/start_pwd/");
        int endIndex = Router_Info.indexOf("/end_info/");
        if (ssidIndex != -1) {
            ssid = "";
            pwd = "";
            for (int i = ssidIndex + 12; i < pwdIndex; i++) {
                ssid += Router_Info[i];
            }
            for (int i = pwdIndex + 11; i < endIndex; i++) {
                pwd += Router_Info[i];
            }
            Serial.println(ssid);
            Serial.println(pwd);
            flg_recv_router_info = 1;
            digitalWrite(WiFi_LED, HIGH);
            // Write the WiFi router's SSID and Password to ESP8266's EEPROM
            Router_Info_EEPROM_Write(ssid, pwd);
        }
        client.flush();
        
        // JSON response
        String s = "HTTP/1.1 200 OK\r\n";
        s += "Content-Type: application/json\r\n\r\n";
        s += "{\"SSID\":\"";
        s += ssid;
        s += "\", \"PASSWORD\":\"";
        s += pwd;
        s += "\"}\r\n";
        s += "\n";
        // Send the response to the client
        client.print(s);
        delay(1);
        Serial.println("Client disconnected");
    }
    else {    /*************** In "STA" mode ****************/
    
        if (WiFi.status() != WL_CONNECTED) {
            Serial.print("****************** Error! WiFi is disconnected! *****************");
            reconnecting();
        }

        long now = millis();
        if (now - lastMsg > upload_interval) {
            lastMsg = now;            
            if(depressed_state == 0) {
                // Wait until user's command is arrived
                Serial.println("Waiting for user's command...");
                Receive_Command();
                // if (command == "starting")
                //     flg_start = 1;            
                // if (command == "waiting") 
                //     flg_start = 0;
            }        
            else if ((depressed_state == 1) || (command == "starting")) { 
                // Start "Zero Waste/Preheat" Routine
                Serial.println("The Zero Waste/Preheat Routine is stated!");
                // Considering that the "Zero Waste/Preheat Routine" is started by App,
                // we need to set depressed_state to 1.
                depressed_state = 1;    
                Get_Temp();
                // Start to count the duration for "Zero Waste/Preheat Rutine"
                if (flg_counter_preheat == false) {
                    preheat_start_time = millis();                
                    flg_counter_preheat = true;
                }    
                            
                if (T_preset_min < T_pump < T_preset_max) {
                    Serial.println("'Zero Waste/Preheat Routine' is done!");
                    digitalWrite(Circ_Pump, LOW);         // Turn off the circulation pump    
                    digitalWrite(START_Led, HIGH);
                    flg_preheat = 1;
                    // send a message "Shower is Ready!" to the server
                    Send_ShoweringData("alarm");

                }
                else {
                    Serial.println("'Zero Waste/Preheat Routine' is running...");
                    digitalWrite(Circ_Pump, HIGH);        // Turn on the circulation pump 
                    digitalWrite(START_Led, LOW);   
                    flg_preheat = 0;
                    // send a message "Shower is not yet Ready." to the server
                    Send_ShoweringData("alarm");
                }            
            }        
            else if (depressed_state == 2) {
                // Start the showering
                Serial.println("The shower is started!");
                // End to count the duration for "Zero Waste/Preheat Rutine"
                if (flg_counter_preheat == true) {
                    preheat_end_time = millis();
                    preheat_cycle = preheat_end_time- preheat_start_time;
                    flg_counter_preheat = false;
                }            
                
                digitalWrite(Outf_Valve, HIGH);     // Turn on the outflow valve to start the showering
                
                // start to count the duration of the shower
                if (flg_counter_shower == false) {
                    shower_start_time = millis();                
                    flg_counter_shower = true;
                }    
                
                // If the shower is working on the "challenge" mode, the timer will be active.
                if (mode == "challenge") {                      
                    if (flg_timer1 == 0) {
                        challenge1.attach((chall_time - 60), handle_ChallengeInterrupt1);
                        flg_timer1 = 1;   
                    }
                    if (flg_timer2 = 1) {
                        challenge1.detach();
                        challenge2.attach(60, handle_ChallengeInterrupt2); 
                        flg_timer2 = 0;                       
                    }                    
                }           
                
                // Adjust the ball of the mixing vavle when the cold or hot button is depreseed by user.
                if (ratio_mix > ratio_mix_last) {   // when the hot buttons is depressed
                    ratio_mix_last = ratio_mix;
                    int offset = ratio_mix - ratio_mix_last;
                    digitalWrite(Mix_Valve_hot, HIGH);
                    delay(offset * 150);
                    digitalWrite(Mix_Valve_hot, LOW);                
                }
                if (ratio_mix < ratio_mix_last) {
                    ratio_mix_last = ratio_mix;
                    int offset = ratio_mix_last - ratio_mix;
                    digitalWrite(Mix_Valve_cold, HIGH);
                    delay(offset * 150);
                    digitalWrite(Mix_Valve_cold, LOW);                
                }
            }
            else if (depressed_state == 3) {
                // Complete the showering 
                Serial.println("The showering is over. It's time to send the showering data to the server.");       
                digitalWrite(Outf_Valve, LOW);     
                digitalWrite(START_Led, LOW);
                
                // End to count the duration of the shower
                if (flg_counter_shower == true) {
                    shower_end_time = millis();
                    shower_cycle = shower_end_time - shower_start_time;
                    flg_counter_shower = false;
                }
                
                // If the shower had just been working on the "challenge", we need to stop the interrupt for the challenge2.
                if (mode == "challenge") 
                    challenge2.detach();

                // send the showering data to server
                Send_ShoweringData("data");

                // lock the device for 5min=300000mS
                delay(300000);    
                ESP.restart();             
            }
        }
        else {
            delay(400);  // Loop function takes about 300ms, so 400 ms is enough.
        }
    }
       
}

