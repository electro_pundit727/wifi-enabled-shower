/*     Simple Stepper Motor Control Exaple Code

       by Dejan Nedelkovski, www.HowToMechatronics.com

*/
// defines pins numbers
int stepPin = 0;  //GPIO0---D3 of Nodemcu--Step of stepper motor driver
int dirPin = 2;   //GPIO2---D4 of Nodemcu--Direction of stepper motor driver

void setup() {
  Serial.begin(115200);
  delay(10);

  // Sets the two pins as Outputs
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  // Currently no stepper motor movement
  digitalWrite(stepPin, LOW);
  digitalWrite(dirPin, LOW);
}

void loop() {
  digitalWrite(dirPin, HIGH); // Enables the motor to move in a particular direction
  // Makes 200 pulses for making one full cycle rotation
  for (int x = 0; x < 200; x++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }
  delay(5000); // One second delay

  digitalWrite(dirPin, LOW); //Changes the rotations direction
  // Makes 400 pulses for making two full cycle rotation
  for (int x = 0; x < 400; x++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }
  delay(5000);
}
