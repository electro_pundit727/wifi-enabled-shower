/*
    Ticker ESP8266
    Hardware: NodeMCU
    Circuits4you.com
    2018
    LED Blinking using Ticker
*/
#include <ESP8266WiFi.h>
#include <Ticker.h>  //Ticker Library

Ticker blinker;
float s = 1;
int q = 0;

#define LED 5  //On board LED

//=======================================================================
void changeState()
{
  digitalWrite(LED, !(digitalRead(LED)));  //Invert Current State of LED
}
//=======================================================================
//                               Setup
//=======================================================================
void setup()
{
  Serial.begin(115200);
  Serial.println("");

  pinMode(LED, OUTPUT);

  //Initialize Ticker every 0.5s
  //blinker.attach(100, changeState); //Use <strong>attach_ms</strong> if you need time in ms
  //blinker.detach();
}
//=======================================================================
//                MAIN LOOP
//=======================================================================
void loop()
{
  long t = millis();

  if (t > 10000) {
    Serial.println(t);
    s = 0.5;
    if (q == 0) {
      blinker.attach(s, changeState); 
      q = 1;
      Serial.println("The interrupt is stated");
      Serial.println(q);
    }
    //    delay(600);
    //    blinker.detach();
    //    delay(5500);
  }

}
//=======================================================================
