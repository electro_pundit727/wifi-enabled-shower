/***************** Summary ******************/
/* In this code we use both of the MQTT protocol and the Http protocol to communicate with the server to 
   achieve the high requirements for the Security and the Certification Systme. 
   MQTT protocol --> The server sends the data (command, mode, and challenge level) to the device using MQTT protocol
                     (server: publish, device:subscribe)                     
   HTTP protocol --> The device sends the data (   ) to the server using Http protocol */

#include <WiFiClient.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#include <Wire.h>
#include <Adafruit_MCP23017.h>
#include <SPI.h>
#include "Adafruit_MAX31855.h"
#include <Ticker.h>

/********************* define Subscribe Topics (excluding the header of the topic) ************************/
const char* shower_command_suffix = "/command";          // command from the server ("start" or "wait")
const char* shower_mode_suffix = "/shower_mode";         // shower mode (“normal”, “challenge”)
//const char* challenge_level_suffix = "/chall_level";     // challenge level in the challenge mode
const char* challenge_time_suffix = "/chall_time";       // challenge time in the challenge mode
const char* prev_shower_temp_suffix = "/prev_shower_temp";         // final temp at the end of the previous shower

/********************* define Publish Topics (excluding the header of the topic) **************************
const char* preheat_cycle_suffix = "/preheat_cycle";     // duration for the "Zero Wasst/Preheat" Routine (in second)
const char* shower_cycle_suffix = "/shower_cycle";       // duration for the shower (in second)
const char* shower_temp_suffix = "/shower_temp";         // temperature at the outflow valve (in Fahrenheit degree)
const char* mixing_temp_suffix = "/mixing_temp";         // temperature at the mixing valve (in Fahrenheit degree)
const char* shower_state = "/state";                     // working state of the shower (“Wait”, “Preheat”, “Shower”, “Over”)

/********************** define PINs ************************/
// ESP8266-12E pins
int MODE_PIN = 3;             // GPIO3 -- pin for switching between AP mode and STA mode 
int esp8266IntPin = 13;       // GPIO13 -- ESP8266-12E Interrupt pin. This pin connected to the interrupt pin on MCP23017.
//int WiFi_LED = 15;          // GPIO15 -- Indicates whether the data was received in AP mode, the device was connected the WiFi router in STA mode.

// MCP 23017 pins
byte WiFi_LED = 3;                 // GPA3 -- Indicates whether the data was received in AP mode, the device was connected the WiFi router in STA mode.
byte Interrupt_START_Butt = 6;     // GPA6 -- START button                
byte Interrupt_Hot_Butt =  5;      // GPA5 -- Hot button 
byte Interrupt_Cold_Butt = 4;      // GPA4 -- Cold button
byte START_Led = 7;                // GPA7 -- START LED
byte Circ_Pump = 11;               // GPB3 -- Circulation Pump
byte Mix_Valve_hot = 10;           // GPB2 -- Mixing vavle relay for Hot water
byte Mix_Valve_cold = 9;           // GPB1 -- Mixing vavle relay for Cold water
byte Outf_Valve = 8;               // GPB0 -- Outflow valve

/*********************** define constant values *************************/
// MQTT broker 
const char* mqttServer = "192.168.1.155";        // address of the MQTT broker on the server
const int mqttPort = 1883;                            // MQTT broker prot
const char* mqttUser = "admin";                     // MQTT broker user name
const char* mqttPassword = "qwer1234";     // MQTT broker user password

int T_def_preset = 90;                 // preset temperature for "Zero Waste/Preheat Routine" (85F~95F) only in the Setup mode
int T_preset_min = 85;                  
int T_preset_max = 95;
int upload_interval = 1000;             // Uploading switch state in miliseconds.

/*********************** define global variables ************************/
// name the shower device
String device = "_shower";     
String DeviceName;             
char device_name[25];          // This name will be attached to the header of the each MQTT topic
// WiFi Network info
String ssid, pwd;
char router_ssid[25];
char router_pwd[25];
int s_len, p_len, dn_len;       // length of WiFi router's ssid and password, length of shower device name
int flg_recv_router_info;       // indicate if the device receved the WiFi router info from the mobile app

uint8_t macAddr[6];
String macID;
char MAC_char[18];
int m_len;                     // length of macID

const char* command = "wait";           // command for starting the shower("wait" or "start")
const char* shower_mode = "setup";      // shower mode("setup", "post" and "challenge")
const char* state = "";                 // working state of the shower (“Wait”, “Preheat”, “Shower”, “Over”)

//int chall_level;                          // challenge level in the challenge mode
int chall_time;                           // challenge time in the challenge mode
int flg_timer1 = 0, flg_timer2 = 0;       // active state of the timer for the challenge mode

//int flg_recv_comm;                // indicate if the command is received successfully by device

int depressed_state = 0;       // Number of times the "START" button has been pressed. That indicates the working step of the shower.       
int ratio_mix = 6, ratio_mix_last = 6;     // ratio between the hot and cold water on the mixing valve  
                                           // preset ratio is 6 (60% hot to 40% cold)
double T_pump;                  // temperature of the circulation pump and the shower head
double T_shower;                // temperature of the shower head                           
float T_prev_shower;            // preset temperature for zero waste/preheat routine in Post Setup mode and Challenge mode
                                // This preset temperature is set to the final temp at the end of the previous shower         
int flg_preheat;                // indicate if the "Zero Waste/Preheat Routine" and the shower is completed.
unsigned long preheat_start_time, preheat_end_time,    // start time and end time of "Zero Waste/Preheat Routine"
              shower_start_time, shower_end_time;      // start time and end time of the shower       
long preheat_cycle, shower_cycle;
bool flg_counter_preheat = false;
bool flg_counter_shower = false;

long lastMsg = 0;

char buf_pub_topic[50];
char buf_sub_topic[50];

// char* buf_preheat_cycle = new char[10];
// char* buf_shower_cycle = new char[10];
// char* buf_shower_temp = new char[10];
// char* buf_mixing_temp = new char[10];

/************************* define instances *******************************/
/*** For Adafruit MAX31855 ***/
// We will use two MAX31855 moudles for this project.
// Default connection is using software SPI, but comment and uncomment one of the two examples below to switch between software SPI and hardware SPI:
// Example creating a thermocouple instance with software SPI on any three digital IO pins.
 #define MAXDO 0         // GPIO0
 #define MAXCS1 14       // GPIO14
 #define MAXCS2 12       // GPIO12
 #define MAXCLK  2       // GPIO2
Adafruit_MAX31855 thermocouple1(MAXCLK, MAXCS1, MAXDO);
Adafruit_MAX31855 thermocouple2(MAXCLK, MAXCS2, MAXDO);

// Example creating a thermocouple instance with hardware SPI on a given CS pin.
//#define MAXCS1 13       
//#define MAXCS2 2        
//Adafruit_MAX31855 thermocouple1(MAXCS1);
//Adafruit_MAX31855 thermocouple2(MAXCS2);

/*** For MCP23017 ***/
Adafruit_MCP23017 mcp;

/*** For MCP23017 ***/
Ticker challenge1, challenge2;      // challenge1: timer to count shower time except the last minite
                                    // challenge2: timer to count the last minute
/*** build a server on ESP8266-12E in the AP mode ***/
WiFiServer server(80);

/*** In STA mode we will declare an object of class WiFiClient, which allows to establish a connection to a specific IP and port [1]. 
We will also declare an object of class PubSubClient, which receives as input of the constructor the previously defined WiFiClient.***/
WiFiClient espClient;
PubSubClient client(espClient);

/************************* define functions *******************************/
void Setup_AP();
void Setup_STA();
void handle_ButtInterrupt();
void set_pub_topic(const char* suffix);
void set_sub_topic(const char* suffix);
void callback(char* topic, byte* payload, unsigned int length);

void setup() {
    
    Serial.begin(115200);
    EEPROM.begin(512);

    pinMode(MODE_PIN, INPUT);
//    pinMode(WiFi_LED, OUTPUT);
//    digitalWrite(WiFi_LED, LOW);    
    mcp.begin(); 
    mcp.pinMode(WiFi_LED, OUTPUT);
    mcp.digitalWrite(WiFi_LED, LOW);

    if (digitalRead(MODE_PIN) == HIGH) {
        Setup_AP();
    }
    else {
        Setup_STA();
    }   
    
    // Interrupt pins 
    pinMode(esp8266IntPin, INPUT);
//    mcp.begin(); 
    mcp.setupInterrupts(true, false, LOW);
    
    mcp.pinMode(Interrupt_START_Butt, INPUT);    
    mcp.pullUp(Interrupt_START_Butt, HIGH);
    mcp.setupInterruptPin(Interrupt_START_Butt, CHANGE);
    mcp.pinMode(Interrupt_Hot_Butt, INPUT);
    mcp.pullUp(Interrupt_Hot_Butt, HIGH);
    mcp.setupInterruptPin(Interrupt_Hot_Butt, CHANGE);
    mcp.pinMode(Interrupt_Cold_Butt, INPUT);
    mcp.pullUp(Interrupt_Cold_Butt, HIGH);
    mcp.setupInterruptPin(Interrupt_Cold_Butt, CHANGE);

//    attachInterrupt(esp8266IntPin, handle_ButtInterrupt, FALLING); 
//    mcp.readGPIOAB();            //This will clear interrupts on MCP prior to entering main loop

    // Normal I/O pins
    mcp.pinMode(START_Led, OUTPUT);    
    mcp.pinMode(Circ_Pump, OUTPUT);
    mcp.pinMode(Mix_Valve_hot, OUTPUT);
    mcp.pinMode(Mix_Valve_cold, OUTPUT);
    mcp.pinMode(Outf_Valve, OUTPUT);
        
    mcp.digitalWrite(START_Led, LOW);    
    mcp.digitalWrite(Circ_Pump, LOW);
    mcp.digitalWrite(Outf_Valve, LOW);    
    
    /*** preset the mixing valve to the 60% hot to 40% cold ratio ***/
    // In order to calibrate the mixing valve, move the mixing valve to the full cold position at beginning  
    mcp.digitalWrite(Mix_Valve_hot, LOW);
    mcp.digitalWrite(Mix_Valve_cold, HIGH);
    delay(2000);   // we assume it takes 1.5S to turn the valve ball between the full cold and the full hot possition.
    mcp.digitalWrite(Mix_Valve_cold, LOW);    
    // preset the mixing valve to the 60% hot to 40% cold ratio
    // We assume that it takes 150ms to rotate the valve ball by 10% of the full range.
    mcp.digitalWrite(Mix_Valve_hot, HIGH);
    delay(900);  // motor energising time = 150ms x 6 = 900ms
    mcp.digitalWrite(Mix_Valve_hot, LOW); 

    attachInterrupt(esp8266IntPin, handle_ButtInterrupt, FALLING); 
    mcp.readGPIOAB();

    // Initialize Ticker 
    // challenge1.attach(100, handle_ChallengeInterrupt1);  
    // challenge1.detach(); 
    // challenge2.attach(100, handle_ChallengeInterrupt2);  
    // challenge2.detach(); 

    ESP.wdtDisable();
    ESP.wdtEnable(WDTO_8S);
}

void Setup_AP() {
  
    // configure a AP on ESP8266 and build a server on it
    Serial.println();
    Serial.println("Configuring access point...");

    WiFi.mode(WIFI_AP);

    WiFi.softAPmacAddress(macAddr);
    for (int i = 0; i < sizeof(macAddr); ++i) {
        sprintf(MAC_char, "%s%02x", MAC_char, macAddr[i]);
    }
    Serial.println();
    Serial.println(MAC_char);
    String macID(MAC_char);
    
    Serial.println(macID);

    macID.toUpperCase();

    String AP_NameString = "Shower_" + macID;
    String AP_PassString = "Shower-" + macID;

    DeviceName = macID + device;

    char AP_NameChar[AP_NameString.length() + 1];
    memset(AP_NameChar, 0, AP_NameString.length() + 1);
    for (int i = 0; i < AP_NameString.length(); i++)
        AP_NameChar[i] = AP_NameString.charAt(i);

    char AP_PassChar[AP_PassString.length() + 1];
    memset(AP_PassChar, 0, AP_PassString.length() + 1);
    for (int i = 0; i < AP_PassString.length(); i++)
        AP_PassChar[i] = AP_PassString.charAt(i);

    Serial.println(AP_NameChar);
    Serial.println(AP_PassChar);

    // build an AP
    WiFi.softAP(AP_NameChar, AP_PassChar);

    IPAddress myIP = WiFi.softAPIP();
    Serial.println();
    Serial.print("AP IP address: ");
    Serial.println(myIP);

    server.begin();
    Serial.println("HTTP server started");
}

void Setup_STA() {

    s_len = EEPROM.read(0);
    p_len = EEPROM.read(1);
    dn_len = EEPROM.read(2);
    m_len = EEPROM.read(3);
    
    for (int i = 0; i < s_len; i++) {
        ssid = ssid + char(EEPROM.read(0x04 + i));
    }
    for (int i = 0; i < p_len; i++) {
        pwd = pwd + char(EEPROM.read(0x04 + s_len + i));
    }
    for (int i = 0; i < dn_len; i++) {
        DeviceName = DeviceName + char(EEPROM.read(0x04 + s_len + p_len + i));
    }
    for (int i = 0; i < m_len; i++) {
        macID = macID + char(EEPROM.read(0x04 + s_len + p_len + dn_len + i));
    }

    ssid.toCharArray(router_ssid, s_len + 1);
    pwd.toCharArray(router_pwd, p_len + 1);
    DeviceName.toCharArray(device_name, dn_len + 1);
    macID.toCharArray(MAC_char, m_len + 1);

    WiFi.mode(WIFI_STA);

    Serial.println();
    Serial.print("WiFi Netowork SSID: ");
    Serial.println(router_ssid);
    Serial.print("WiFI Netowork Password: ");
    Serial.println(router_pwd);
    Serial.print("Shower Device Name: ");
    Serial.println(device_name);
    Serial.print("MAC ID: ");
    Serial.println(MAC_char);

    int attempt = 0;
    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(router_ssid);

    WiFi.begin(router_ssid, router_pwd);

    while (WiFi.status() != WL_CONNECTED) {
        if (attempt < 30)
            attempt ++;
        else
            ESP.restart();
        delay(500);
        Serial.print(".");
        mcp.digitalWrite(WiFi_LED, !mcp.digitalRead(WiFi_LED));
    }

    mcp.digitalWrite(WiFi_LED, HIGH);
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    
    client.setServer(mqttServer, mqttPort);
    client.setCallback(callback);    
}

void Router_Info_EEPROM_Write(String ssid, String pwd, String DeviceName, String macID) {

    s_len = ssid.length();
    p_len = pwd.length();
    dn_len = DeviceName.length();
    m_len = macID.length();
    
    EEPROM.write(0, s_len);
    EEPROM.write(1, p_len);
    EEPROM.write(2, dn_len);
    EEPROM.write(3, m_len);

    for (int i = 0; i < s_len; i++) {
        EEPROM.write(0x04 + i, ssid[i]);
    }
    for (int i = 0; i < p_len; i++) {
        EEPROM.write(0x04 + s_len + i, pwd[i]);
    }
    for (int i = 0; i < dn_len; i++) {
        EEPROM.write(0x04 + s_len + p_len + i, DeviceName[i]);
    }
    for (int i = 0; i < m_len; i++) {
        EEPROM.write(0x04 + s_len + p_len + dn_len + i, macID[i]);
    }
    EEPROM.commit();
    Serial.println("The SSID and Password of the WiFi router, the shower device name are saved in the EEPROM.");
}

void Get_Temp() {

    // basic readout test, just print the current temp
    // Serial.print("Internal Temp1 = ");
    // Serial.println(thermocouple1.readInternal());
    // Serial.print("Internal Temp2 = ");
    // Serial.println(thermocouple2.readInternal());

    // T_pump = thermocouple1.readCelsius();
    // T_shower = thermocouple2.readCelsius();    
    T_pump = thermocouple1.readFarenheit();
    T_shower = thermocouple2.readFarenheit();

    if (isnan(T_pump)) {
        Serial.println("Something wrong with thermocouple on the circulation pump.");
    } 
    else {
        Serial.print("T_pump = "); 
        Serial.print(T_pump);
        Serial.println("F");
    }
    if (isnan(T_shower)) {
        Serial.println("Something wrong with thermocouple on the mixing valve");
    } 
    else {
        Serial.print("T_shower = "); 
        Serial.print(T_shower);
        Serial.println("F");
    }    
    delay(10);
}

void handle_ButtInterrupt() {  /* We can add a code to limit the interrupt inteval time */

    Serial.println("************************");  
    Serial.println("Interrupt is just detected!");
    uint8_t pin = mcp.getLastInterruptPin();
    uint8_t val = mcp.getLastInterruptPinValue();

    if ((pin == Interrupt_START_Butt) && (val == LOW)) {
        Serial.println("START button is depressed!");
        depressed_state ++;
        if ((depressed_state == 2) && (flg_preheat == 0)) {
            Serial.println("Error! The 'Zero Waste/Preheat Routine' has not yet finished. Please wait for a while.");
            depressed_state = 1;   
        }
        if (depressed_state > 3) 
            depressed_state = 3;       
    }
    if ((pin == Interrupt_Hot_Butt) && (val == LOW)) {     
        // The ratio can be changed only in the showering.
        if (depressed_state == 2) {
            Serial.println("Hot button is depressed. The hot water ratio will be incresed by 10 percent!");
            ratio_mix ++;            
            if (ratio_mix > 10) 
                ratio_mix = 10; 
            Serial.print("Current mixing ratio: ");                            
            Serial.print("hot->");
            Serial.print(ratio_mix * 10);
            Serial.print("percent, ");
            Serial.print("cold->");
            Serial.print((10 - ratio_mix) * 10);
            Serial.println("percent");                             
        }
        else {
            Serial.println("Error! You can setup the mixing ratio only while showering.");
        }
    }
    if ((pin == Interrupt_Cold_Butt) && (val == LOW)) {        
        // The ratio can be changed only in the showering.
        if (depressed_state == 2) {
            Serial.println("Cold button is depressed. The cold water ratio will be decreased by 10 percent!");
            ratio_mix --;
            if (ratio_mix < 0) 
                ratio_mix = 0;                             
            Serial.print("Current mixing ratio: ");                            
            Serial.print("hot->");
            Serial.print(ratio_mix * 10);
            Serial.print("percent, ");
            Serial.print("cold->");
            Serial.print((10 - ratio_mix) * 10);
            Serial.println("percent");
        }
        else {
            Serial.println("Error! You can setup the mixing ratio only while showering.");
        }
    }
    mcp.readGPIOAB();
    Serial.println("************************");          
}

void handle_ChallengeInterrupt1() {
    
    // This interrupt will be called when the timer comes within 1 minite of completion in the Challenge Routine mode
    Serial.println(" Hurry up! The shower will be finished automatically in a minute.");
    // close the outflow valve for 2 seconds, and then open the valve again.
    mcp.digitalWrite(Outf_Valve, LOW);
    
    flg_timer2 = 1;
    Serial.print("flg_timer2= ");    
    Serial.println(flg_timer2);
}

void handle_ChallengeInterrupt2() {
    
    Serial.println("The challenge mode is over!");
    mcp.digitalWrite(Outf_Valve, LOW);
    mcp.digitalWrite(START_Led, LOW);
    
    depressed_state ++;      // Then depressed_state will be 3.
    state = "Over";
}

void callback(char* topic, byte* payload, unsigned int length) {
  
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
    }
    Serial.println("");

    // Check topic of the shower start command from the App
    set_sub_topic(shower_command_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

        if (!strncmp((const char*)payload, "start", 5)) {
            Serial.println("Start command is arrived from App!");
            command = "start";
        }
        else if (!strncmp((const char*)payload, "wait", 4)) {
            Serial.println("Wait command is arrived from App!");
            command = "wait";
        }
    }    
    else {
        // Check topic of the shower mode from the App
        set_sub_topic(shower_mode_suffix);
        if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

            if (!strncmp((const char*)payload, "setup", 5)) {
                Serial.println("The setup mode is selected by App!");
                shower_mode = "setup";
            }
            else if (!strncmp((const char*)payload, "post", 4)) {
                Serial.println("The post mode is selected by App!");
                shower_mode = "post";
            }
            else if (!strncmp((const char*)payload, "challenge", 9)) {
                Serial.println("The challenge mode is selected by App!");
                shower_mode = "challenge";                
            }
        }
    }    
    // // Check topic of the challenge level from the App
    // set_sub_topic(challenge_level_suffix);
    // if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

    //     char* val = (char*)payload;
    //     chall_level = atoi(val);
    //     Serial.print("The challenge level is  ");
    //     Serial.println(chall_level);      
    // }

    // Check topic of the challenge time from the App
    set_sub_topic(challenge_time_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

        char* val = (char*)payload;
        chall_time = atoi(val);
        Serial.print("The challenge time is  ");
        Serial.println(chall_time);      
    }    
    else {
        // Check topic of the shower_temp from the App
        set_sub_topic(prev_shower_temp_suffix);
        if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {
                
            char* val = (char*)payload;
            T_prev_shower = atof(val);
            Serial.print("The preset temperature for the Zero Waste/Preheat routine is ");
            Serial.println(T_prev_shower);            
        }
    }
}

void Send_ShoweringData(String type) {

    // convert the showering data as JSON format
    StaticJsonBuffer<300> JSONbuffer;   // declaring static JSON buffer
    JsonObject& JSONencoder = JSONbuffer.createObject();
    if (type == "data") {
        JSONencoder["mac_id"] = macID;
        JSONencoder["preheat_cycle"] = preheat_cycle;
        JSONencoder["shower_cycle"] = shower_cycle;
        Get_Temp();  // get the final temperature at the end of the shower
        JSONencoder["shower_temp"] = T_shower;        
        JSONencoder["state"] = state;        
    } 
    else if (type == "alarm") {
        if (flg_preheat == 1) {
            JSONencoder["mac_id"] = macID;
            JSONencoder["alarm"] = "Shower is ready!";               
        }   
        else {
            JSONencoder["mac_id"] = macID;
            JSONencoder["alarm"] = "Shower is not ready yet.";
        }
    }
    char JSONmessageBuffer[300];
    JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    Serial.println(JSONmessageBuffer);

    // send the sensor data to the sever
    HTTPClient http;    // declare object of class HTTPClient
    http.begin("http://192.168.1.155:8000/api/device_status/");                                                              
    http.addHeader("Content-Type", "application/json");       

    int httpCode = http.POST(JSONmessageBuffer);             // Send the request
    String payload = http.getString();                       // Get the response payload                           
    
    if (httpCode == HTTP_CODE_OK) {      
        Serial.println(httpCode);   //Print HTTP return code
        Serial.println("The showering data is posted to the sever successfully.");
    }
    else {
        Serial.println(httpCode);
        Serial.println("The posting to the server faile! Check Internet Connection or Server Error.");
        Serial.println(payload);    //Print request response payload
    }      
    http.end();  //Close connection
}

void reconnect() {

    int attempt = 0;
    // Loop until we're reconnected
    while (!client.connected()) {
        if (attempt < 3)
            attempt ++;
        else
            ESP.restart();

        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        if (client.connect(MAC_char, mqttUser, mqttPassword)) {

            Serial.println("connected");
            // Once connected, publish an announcement...
            client.publish("overall_topic", "hello world");
            // ... and resubscribe  
            set_sub_topic(shower_command_suffix);
            client.subscribe(buf_sub_topic);

            set_sub_topic(shower_mode_suffix);
            client.subscribe(buf_sub_topic);

            // set_sub_topic(challenge_level_suffix);
            // client.subscribe(buf_sub_topic);

            set_sub_topic(prev_shower_temp_suffix);
            client.subscribe(buf_sub_topic);

            set_sub_topic(challenge_time_suffix);
            client.subscribe(buf_sub_topic);
            //client.subscribe("common");
        } 
        else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 3 seconds before retrying
            delay(3000);
        }
    }
}

void loop() {

    if (digitalRead(MODE_PIN) == HIGH) {  /************ In the "AP" mode *************/

        if (flg_recv_router_info == 0) {
            mcp.digitalWrite(WiFi_LED, !mcp.digitalRead(WiFi_LED));
            delay(100);
        }
        WiFiClient client = server.available();
        if (!client) {
            return;
        }
        // receive the data from the mobile app
        String Router_Info = client.readStringUntil('\r');  
        Serial.println(Router_Info);
        client.flush();

        // Parse the received data and get the ssid and password of the AP router
        int ssidIndex = Router_Info.indexOf("/start_ssid/");
        int pwdIndex = Router_Info.indexOf("/start_pwd/");
        int endIndex = Router_Info.indexOf("/end_info/");
        if (ssidIndex != -1) {
            ssid = "";
            pwd = "";
            for (int i = ssidIndex + 12; i < pwdIndex; i++) {
                ssid += Router_Info[i];
            }
            for (int i = pwdIndex + 11; i < endIndex; i++) {
                pwd += Router_Info[i];
            }
            Serial.print("WiFi Network SSID: ");
            Serial.println(ssid);
            Serial.print("WiFi Network PASSWORD: ");
            Serial.println(pwd);
            flg_recv_router_info = 1;
            mcp.digitalWrite(WiFi_LED, HIGH);
            // Write the WiFi router's SSID and Password to ESP8266's EEPROM
            String macID(MAC_char);
            Router_Info_EEPROM_Write(ssid, pwd, DeviceName, macID);
        }
        client.flush();
        
        // JSON response
        String s = "HTTP/1.1 200 OK\r\n";
        s += "Content-Type: application/json\r\n\r\n";
        s += "{\"SSID\":\"";
        s += ssid;
        s += "\", \"PASSWORD\":\"";
        s += pwd;
        s += "\"}\r\n";
        s += "\n";
        // Send the response to the client
        client.print(s);
        delay(1);
        Serial.println("Client disconnected");
    }
    else {  /************ In "STA" mode ************/
        if (WiFi.status() != WL_CONNECTED) {
            Serial.print("WiFi is disconnected! The device will be restarted soon.");
            ESP.restart();
        }   
        if (!client.connected()) {
            reconnect();            
        }
        client.loop();

        long now = millis();
        if (now - lastMsg > upload_interval) {
            lastMsg = now;            
            if(depressed_state == 0) {        /*Wait mode*/
                // Wait until user's command is arrived
                Serial.println("Waiting for user's command...");
                state = "Wait";
                if (command == "start") {
                    depressed_state = 1;
                    Serial.println("The start commnad is received from the App");
                }                
            }        
            else if (depressed_state == 1) {        /*"Zero Waste/Preheat" Routine*/
                // Start "Zero Waste/Preheat" Routine
                Serial.println("The Zero Waste/Preheat Routine is stated!");                
                state = "Preheat";                
                Get_Temp();
                // Start to count the duration for "Zero Waste/Preheat Rutine"
                if (flg_counter_preheat == false) {
                    Serial.print("################## ");
                    Serial.print("The countdown of the Preheat routine has started!");
                    Serial.println(" #################");
                    preheat_start_time = millis();                
                    flg_counter_preheat = true;
                }    
                            
                if (shower_mode == "setup") {
//                    Serial.println("Current shower mode is setup mode");
//                    Serial.print("flg_preheat= ");
//                    Serial.print(flg_preheat);
//                    Serial.print("   T_def_preset= ");
//                    Serial.print(T_def_preset);
//                    Serial.print("   T_pump= ");
//                    Serial.println(T_pump);

                    // // Start to count the duration for "Zero Waste/Preheat Rutine"
                    // // we need to count this duration only in the setup mode
                    // if (flg_counter_preheat == false) {
                    //     preheat_start_time = millis();                
                    //     flg_counter_preheat = true;
                    // }    

                    // The preset temp is set to the default presete temp in the setup mode.                                       
                    if (T_pump >= T_def_preset) {
                        //Serial.println("'Zero Waste/Preheat Routine' is done!");
                        mcp.digitalWrite(Circ_Pump, LOW);         // Turn off the circulation pump    
                        mcp.digitalWrite(START_Led, HIGH);
                        flg_preheat = 1;
                        // send a message "Shower is Ready!" to the server
                        Send_ShoweringData("alarm");
                    }
                    else if (T_pump > T_preset_min) {
                        Serial.println("'Zero Waste/Preheat Routine' is done!");
                        mcp.digitalWrite(START_Led, HIGH);
                        flg_preheat = 1;
                        // send a message "Shower is Ready!" to the server
                        Send_ShoweringData("alarm");
                    }
                    else if (T_pump < T_preset_min) {
                        Serial.println("'Zero Waste/Preheat Routine' is still running...");
                        flg_preheat = 0;
                        mcp.digitalWrite(Circ_Pump, HIGH);        // Turn on the circulation pump 
                        mcp.digitalWrite(START_Led, LOW);                                     
                        // send a message "Shower is not yet Ready." to the server
                        Send_ShoweringData("alarm");
                    }            
                }
                if ((shower_mode == "post") || (shower_mode == "challenge")) {
                    // The preset temp is set to the final temp at the end of the previuos shower in the post and the challenge mode.
                    if (T_pump > T_prev_shower) {
                        Serial.println("'Zero Waste/Preheat Routine' is done!");
                        mcp.digitalWrite(Circ_Pump, LOW);         // Turn off the circulation pump    
                        mcp.digitalWrite(START_Led, HIGH);
                        flg_preheat = 1;
                        // send a message "Shower is Ready!" to the server
                        Send_ShoweringData("alarm");
                    }
                    else if (T_pump < T_preset_min) {
                        Serial.println("'Zero Waste/Preheat Routine' is still running...");
                        flg_preheat = 0;
                        mcp.digitalWrite(Circ_Pump, HIGH);        // Turn on the circulation pump 
                        mcp.digitalWrite(START_Led, LOW);                       
                        // send a message "Shower is not yet Ready." to the server
                        Send_ShoweringData("alarm");
                    }            
                }
            }        
            else if (depressed_state == 2) {          /*Shower mode*/
                // Start the showering
                state = "Shower";
                Serial.println("The shower is started!");
                                
                // End to count the duration for "Zero Waste/Preheat Rutine"
                if (flg_counter_preheat == true) {                  
                    mcp.digitalWrite(Circ_Pump, LOW);         // Turn off the circulation pump
                    
                    preheat_end_time = millis();
                    preheat_cycle = (preheat_end_time- preheat_start_time) / 1000;
                    flg_counter_preheat = false;
                    Serial.print("################## ");
                    Serial.print("The countdown of the Preheat routine has ended!");
                    Serial.println(" #################");
                    Serial.print("preheat cycle: ");
                    Serial.println(preheat_cycle);
                }
                                                                          
                if (state == "Shower") 
                    mcp.digitalWrite(Outf_Valve, HIGH);     // Turn on the outflow valve to start the showering
                
                // start to count the duration of the shower
                if (flg_counter_shower == false) {
                    Serial.print("$$$$$$$$$$$$$$$$$$$ ");
                    Serial.print("The countdown of the Shower cycle has started!");
                    Serial.println(" $$$$$$$$$$$$$$$$$$$");
                    shower_start_time = millis();                
                    flg_counter_shower = true;
                }    
                
                // If the shower is working on the "challenge" mode, the timer will be active.                
                if (shower_mode == "challenge") {                      
                    if (flg_timer1 == 0) {
                        challenge1.attach((chall_time - 60), handle_ChallengeInterrupt1);
                        flg_timer1 = 1;   
                    }
                    if (flg_timer2 == 1) {
                        challenge1.detach();
                        // close the outflow valve for 2 seconds, and then open the valve again.
                        delay(2000);
                        mcp.digitalWrite(Outf_Valve, HIGH);
                        challenge2.attach(60, handle_ChallengeInterrupt2); 
                        flg_timer2 = 0;                       
                    }                    
                }           
                
                // Adjust the ball of the mixing vavle when the cold or hot button is depreseed by user.
                if (ratio_mix > ratio_mix_last) {   // when the hot buttons is depressed
                    int offset = ratio_mix - ratio_mix_last;
                    
                    Serial.print("ratio_mix: ");
                    Serial.print(ratio_mix);
                    Serial.print("  ratio_mix_last: ");
                    Serial.println(ratio_mix_last);
                    
                    ratio_mix_last = ratio_mix;
                    mcp.digitalWrite(Mix_Valve_hot, HIGH);
                    delay(offset * 150);
                    mcp.digitalWrite(Mix_Valve_hot, LOW);                                    
                }
                if (ratio_mix < ratio_mix_last) {
                    int offset = ratio_mix_last - ratio_mix;
                    
                    Serial.print("ratio_mix: ");
                    Serial.print(ratio_mix);
                    Serial.print("  ratio_mix_last: ");
                    Serial.println(ratio_mix_last);
                    
                    ratio_mix_last = ratio_mix;
                    mcp.digitalWrite(Mix_Valve_cold, HIGH);
                    delay(offset * 150);
                    mcp.digitalWrite(Mix_Valve_cold, LOW);        
                }                
            }
            else if (depressed_state == 3) {      /*Shower is over*/
                // Complete the showering 
                Serial.println("The showering is over. It's time to send the showering data to the server."); 
                state = "Over";
                mcp.digitalWrite(Outf_Valve, LOW);     
                mcp.digitalWrite(START_Led, LOW);
                
                // End to count the duration of the shower
                if (flg_counter_shower == true) {
                    shower_end_time = millis();
                    shower_cycle = (shower_end_time - shower_start_time) / 1000;
                    flg_counter_shower = false;
                    Serial.print("$$$$$$$$$$$$$$$$$$$ ");
                    Serial.print("The countdown of the Shower cycle has ended!");
                    Serial.println(" $$$$$$$$$$$$$$$$$$$");
                }
                
                // If the shower had just been working on the "challenge", we need to stop the interrupt for the challenge2.
                if (shower_mode == "challenge") 
                    challenge2.detach();

                // send the showering data to server
                Send_ShoweringData("data");

                // lock the device for 5min=300000mS
                if (shower_mode == "challnege")
                    delay(300000);    
                ESP.restart();             
            }
        }
        else {
            delay(400);  // Loop function takes about 300ms, so 400 ms is enough.
        }
    }
}

void set_pub_topic(const char* suffix) {

    int len1 = strlen(device_name);
    int len2 = strlen(suffix);
    for (int i = 0; i < len1 + len2; i++) {
        if (i < len1)
        buf_pub_topic[i] = device_name[i];
        else
        buf_pub_topic[i] = suffix[i - len1];
    }
    buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix) {
    
    int len1 = strlen(device_name);
    int len2 = strlen(suffix);
    for (int i = 0; i < len1 + len2; i++) {
        if (i < len1)
        buf_sub_topic[i] = device_name[i];
        else
        buf_sub_topic[i] = suffix[i - len1];
    }
    buf_sub_topic[len1 + len2] = '\0';
}



