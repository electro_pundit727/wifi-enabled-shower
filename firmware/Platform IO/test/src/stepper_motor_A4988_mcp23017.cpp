#include <Adafruit_MCP23017.h>
#include <SPI.h>

// MCP 23017 pins
byte stepPin = 9;  // GPB1 of MCP23017 -- motor step pin for the stepper motor
byte dirPin = 8;   // GPB0 of MCP23017 -- direction pin for the stepper motor

/*** For MCP23017 ***/
Adafruit_MCP23017 mcp;
     
void setup() {
    Serial.begin(115200);
    delay(10);

    mcp.begin(); 
    // Sets the two pins as Outputs
    mcp.pinMode(stepPin, OUTPUT); 
    mcp.pinMode(dirPin, OUTPUT);
    // Currently no stepper motor movement
    mcp.digitalWrite(stepPin, LOW);
    mcp.digitalWrite(dirPin, LOW);
    mcp.readGPIOAB();
}

void loop() {
    digitalWrite(dirPin,HIGH); // Enables the motor to move in a particular direction
    // Makes 200 pulses for making one full cycle rotation
    for(int x = 0; x < 200; x++) {
    mcp.digitalWrite(stepPin, HIGH); 
    delayMicroseconds(500); 
    mcp.digitalWrite(stepPin, LOW); 
    delayMicroseconds(500); 
    }
    delay(5000); // One second delay
    
    digitalWrite(dirPin, LOW); //Changes the rotations direction
    // Makes 400 pulses for making two full cycle rotation
    for(int x = 0; x < 400; x++) {
    mcp.digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    mcp.digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
    }
    delay(5000);
}