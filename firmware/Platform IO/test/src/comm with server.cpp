#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

const char* ssid = "TP-LINK-REAL3D";                    // type the SSID of your wifi network router
const char* password = "dkanehahffk";            // type the password of your wifi network router
const char* host = "192.168.1.151:5000";                        // server address to post sensor data
const char* sensor_name = "HC-SR04 UltraSonic sensor";

const char* command = "start";  
int chall_level;                // challenge level in the challenge mode
int f_succ;                     // flag to indicate if the command is received successfully by device

void setup_wifi() {
  
  WiFi.mode(WIFI_STA);    // set up esp8266 as the station mode

  int attempt = 0;
  delay(10);              // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
    delay(500);
    Serial.print(".");
    // digitalWrite(wifi_led, !digitalRead(wifi_led));
  }

  if (WiFi.status() == WL_CONNECTED) {
    // digitalWrite(wifi_led, HIGH);
    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
}

void setup() {

  Serial.begin(115200);
  WiFi.begin(ssid,password);  // add ssid and password here               

  setup_wifi();      
}

void reconnecting() {
  
  Serial.println("Now is trying to reconnect...");
  // Loop until we're reconnected
  int attempt = 0;
  while (WiFi.status() != WL_CONNECTED) {
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
    delay(500);
    Serial.print(".");
    // digitalWrite(wifi_led, !digitalRead(wifi_led));
  }
    // digitalWrite(wifi_led, HIGH);
  Serial.println("");
  Serial.println("WiFi reconnected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void Send_ShoweringData(String type);
void Receive_Command();
void Parsing_Command(String cmd);

void loop() {
  
  if (WiFi.status() == WL_CONNECTED) {
    // Send the showering data or a alarm message to the server.
    Send_ShoweringData("Alarm");  

    // Receive the commands from the sever
    Receive_Command();   
  }
  else {
    Serial.println("Error in WiFi connection.");
    reconnecting();
  }
  delay(2000);
}

void Receive_Command() {
  
  HTTPClient http;    // declare object of class HTTPClient
  http.begin("http://192.168.1.151:5000/command");  

  int httpCode = http.GET();
  String payload = http.getString();
  Serial.print("The received data: ");
  Serial.println(payload);

  if (httpCode == HTTP_CODE_OK) {
    f_succ = 1;
    Serial.println("The command is received successfully!");
    Send_ShoweringData("response"); 
    //Parse the received data
    Parsing_Command(payload);
  }
  else {
    f_succ = 0;
    Serial.println(httpCode);
    Serial.println("The command not receved successfully. Check Internet Connection or Server Error.");
    Send_ShoweringData("response"); 
  }      
  http.end();  //Close connection
}

void Parsing_Command(String cmd) {

  // const size_t bufferSize = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(8) + 370;
  // DynamicJsonBuffer jsonBuffer(bufferSize);
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(cmd);
  // Parameters
  command = root["command"]; 
  chall_level = root["challenge level"];

  Serial.print("command: ");
  Serial.println(command);
  Serial.print("challenge level: ");
  Serial.println(chall_level);
}

void Send_ShoweringData(String type) {

  // convert the showering data as JSON format
  StaticJsonBuffer<300> JSONbuffer;   // declaring static JSON buffer
  JsonObject& JSONencoder = JSONbuffer.createObject();
  if (type == "data") {
    
    JSONencoder["Preheat cycle"] = 500;
    JSONencoder["Duration of the shower"] = 600;
    JSONencoder["Gallens used"] = 700;
  } 
  else if (type == "Alarm") {
    JSONencoder["Alarm"] = "Shower is Ready";
  }
  else if (type == "response") {
    if (f_succ == 1)
      JSONencoder["Comm_response"] = "Success";
    else
      JSONencoder["Comm_response"] = "Failure";
  } 

  char JSONmessageBuffer[300];
  JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
  Serial.println(JSONmessageBuffer);

  // send the sensor data to the sever
  HTTPClient http;    // declare object of class HTTPClient
  http.begin("http://192.168.1.151:5000/profile");                                                              
  http.addHeader("Content-Type", "application/json");       

  int httpCode = http.POST(JSONmessageBuffer);             // Send the request
  String payload = http.getString();                       // Get the response payload                           
  
  if (httpCode == HTTP_CODE_OK) {      
    Serial.println(httpCode);   //Print HTTP return code
    Serial.println("The showering data is posted to the sever successfully.");
  }
  else {
    Serial.println(httpCode);
    Serial.println("The posting to the server faile! Check Internet Connection or Server Error.");
    Serial.println(payload);    //Print request response payload
  }      
  http.end();  //Close connection
}