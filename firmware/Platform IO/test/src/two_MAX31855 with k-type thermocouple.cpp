/*************************************************** 
  This is an example for the Adafruit Thermocouple Sensor w/MAX31855K

  Designed specifically to work with the Adafruit Thermocouple Sensor
  ----> https://www.adafruit.com/products/269

  These displays use SPI to communicate, 3 pins are required to  
  interface
  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include <SPI.h>
#include "Adafruit_MAX31855.h"

// Default connection is using software SPI, but comment and uncomment one of
// the two examples below to switch between software SPI and hardware SPI:

// Example creating a thermocouple instance with software SPI on any three
// digital IO pins.
// #define MAXDO   3
// #define MAXCS   4
// #define MAXCLK  5

// initialize the Thermocouple
// Adafruit_MAX31855 thermocouple(MAXCLK, MAXCS, MAXDO);

// Example creating a thermocouple instance with hardware SPI
// on a given CS pin.
#define MAXCS1 5       // use GPIO5 for first chip's CS signal  
#define MAXCS2 4       // use GPIO4 for second chip's CS signal  

Adafruit_MAX31855 thermocouple1(MAXCS1);
Adafruit_MAX31855 thermocouple2(MAXCS2);

void setup() {
  Serial.begin(115200);
 
  while (!Serial) delay(1); // wait for Serial on Leonardo/Zero, etc

  Serial.println("Two MAX31855s test");
  // wait for MAX chip to stabilize
  delay(500);
}

void loop() {
  // basic readout test, just print the current temp
   Serial.print("Internal Temp1 = ");
   Serial.println(thermocouple1.readInternal());

   Serial.print("Internal Temp2 = ");
   Serial.println(thermocouple2.readInternal());

   double c1 = thermocouple1.readCelsius();
   double c2 = thermocouple2.readCelsius();
   if (isnan(c1)) {
     Serial.println("Something wrong with thermocouple1!");
   } 
   else {
     Serial.print("C1 = "); 
     Serial.println(c1);
   }
   if (isnan(c2)) {
     Serial.println("Something wrong with thermocouple2!");
   } 
   else {
     Serial.print("C2 = "); 
     Serial.println(c2);
   }
   //Serial.print("F = ");
   //Serial.println(thermocouple.readFarenheit());
 
   delay(1000);
}